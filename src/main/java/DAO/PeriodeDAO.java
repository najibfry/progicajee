package DAO;

import metier.EquipementService;
import metier.Periode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PeriodeDAO extends DAO<Periode> {
    public PeriodeDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Periode getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Periode> getAll() {
        ResultSet rs;
        ArrayList<Periode> liste = new ArrayList<Periode>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_periode,numero_semaine \n" +
                    "from Periode " +
                    "order by numero_semaine";

            rs = stmt.executeQuery(strCmd);

           Periode periode;

            while (rs.next())
            {
                periode = new Periode(rs.getInt(1),rs.getString(2));
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données période impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Periode objet) {
        return false;
    }

    @Override
    public boolean update(Periode objet) {
        return false;
    }

    @Override
    public boolean delete(Periode objet) {
        return false;
    }
}
