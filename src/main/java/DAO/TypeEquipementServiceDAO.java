package DAO;

import metier.EquipementService;
import metier.TypeEquipement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypeEquipementServiceDAO extends DAO<TypeEquipement>{
    public TypeEquipementServiceDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypeEquipement getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypeEquipement> getAll()

    {
        ResultSet rs;
        ArrayList<TypeEquipement> liste = new ArrayList< TypeEquipement>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_type_service,libelle, from type_equipement_service  order by libelle";
   TypeEquipement typeEquipement;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
            typeEquipement= new TypeEquipement(rs.getInt(1), rs.getString(2));
            liste.add(typeEquipement);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données Type services et equipements impossible  "+ error);
        }
        return liste;

    }

    @Override
    public boolean insert(TypeEquipement objet) {
        return false;
    }

    @Override
    public boolean update(TypeEquipement objet) {
        return false;
    }

    @Override
    public boolean delete(TypeEquipement objet) {
        return false;
    }
}
