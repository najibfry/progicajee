package DAO;

import metier.Personne;
import metier.Prestation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PersonneDAO extends DAO<Personne>{

    public PersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Personne getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Personne> getAll() {

        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<Personne>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_personne,nom_personne,prenom_personne, email" +
                    " from personne order by nom_personne";
            Personne personne;
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {;
                personne= new Personne(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4));
                liste.add(personne );
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;


    }

    @Override
    public boolean insert(Personne objet) {
        return false;
    }

    @Override
    public boolean update(Personne objet) {
        return false;
    }

    @Override
    public boolean delete(Personne objet) {
        return false;
    }
}
