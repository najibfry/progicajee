package DAO;

import metier.Departement;
import metier.Region;
import metier.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class RegionDAO extends DAO<Region>{
    public RegionDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Region getByID(int id) {
        return null;
    }


    public ArrayList<Ville> addListCodeRegion(String codeRegion){
        ResultSet rs;
        ArrayList <Ville> list = new ArrayList<Ville>();
    try {
        Statement stmt = null;

        stmt = connexion.createStatement();
        String strCmd = "SELECT c.code_insee,c.nom_commune,c.code_postale,c.latitude,c.longitude from region as r\n" +
                "                    join Departement as d on d.code_region=r.code_region\n" +
                "join commune as c on c.code_departement = d.code_departement" +
                "                   where r.code_region like '"+codeRegion+"' order by nom_region";

        rs = stmt.executeQuery(strCmd);
        Region region = new Region(null, "Region");
        list.add(new Ville(null, "Ville", "", 0, 0));

        while (rs.next()) {
               list.add(new Ville(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getFloat(4), rs.getFloat(5)));
        }
        rs.close();
        stmt.close();}
        catch(Exception error){
            System.out.println("récuperation des villes de la Region impossible  " + error);
        }
    return  list;
    }







    @Override
    public ArrayList<Region> getAll() {
        ResultSet rs;
        ArrayList<Region> liste = new ArrayList<Region>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  r.code_region, r.nom_region, d.code_departement, d.nom_departement" +
                    "  from region as r join Departement as d on d.code_region=r.code_region\n" +
                    "                    order by nom_region";

            rs = stmt.executeQuery(strCmd);


           Region  region =new Region(null, "Region");
           region.getDepartementlistOfR().add(new Departement(null,"Departement"));
            liste.add(0,region);
            while (rs.next())
              {    if (!(liste.get(liste.size()-1).getNomRegion()).equals(rs.getString(2)))
                  { region = new Region(rs.getString(1), rs.getString(2));
                  liste.add(region);
                  region.getDepartementlistOfR().add(new Departement(rs.getString(3),rs.getString(4)));
                  }

              else
                  region.getDepartementlistOfR().add(new Departement(rs.getString(3),rs.getString(4)));

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données Region impossible  "+ error);
        }

        return liste;
    }

    @Override
    public boolean insert(Region objet) {
        return false;
    }

    @Override
    public boolean update(Region objet) {
        return false;
    }

    @Override
    public boolean delete(Region objet) {
        return false;
    }
}
