package DAO;

import metier.Departement;
import metier.Region;
import metier.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DepartementDAO extends DAO<Departement> {
    public DepartementDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Departement getByID(int id) {
        return null;
    }

    public ArrayList<Ville> addvilleListofDepartement(String codeDepartement){
        ResultSet rs;
        ArrayList <Ville> list = new ArrayList<Ville>();
        try {
            Statement stmt = null;

            stmt = connexion.createStatement();
            String strCmd = "SELECT c.code_insee,c.nom_commune,c.code_postale,c.latitude,c.longitude from region as r\n" +
                    "                    join Departement as d on d.code_region=r.code_region\n" +
                    "join commune as c on c.code_departement = d.code_departement" +
                    "                   where r.code_region like '"+codeDepartement+"' order by nom_region";

            rs = stmt.executeQuery(strCmd);
            Departement departement;
            list.add(new Ville(null, "Ville", "", 0, 0));

            while (rs.next()) {
                list.add(new Ville(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getFloat(4), rs.getFloat(5)));
            }
            rs.close();
            stmt.close();}
        catch(Exception error){
            System.out.println("récuperation des villes du departement impossible  " + error);
        }
        return  list;
    }
    @Override
    public ArrayList<Departement> getAll() {
        ResultSet rs;
        ArrayList<Departement> liste = new ArrayList<Departement>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  d.code_departement,d.nom_departement,c2.code_insee,c2.nom_commune\n" +
                    "                    ,c2.code_postale,c2.latitude,c2.longitude,r.code_region,r.nom_region\n" +
                    "                    from departement as D join commune c2 on D.code_departement = c2.code_departement\n" +
                    "                    join region as r on r.code_region = d.code_region\n" +
                    "                     order by nom_departement";

            rs = stmt.executeQuery(strCmd);


            Departement  departement =new Departement(null,"Departement") ;
            liste.add(0, departement);

            while (rs.next())
            {   if ( !liste.get(liste.size()-1).getNomDepartement().equals(rs.getString(2))){
             departement = new Departement(rs.getString(1), rs.getString(2));
                departement.getVilleListOfD().add(new Ville(rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getFloat(6), rs.getFloat(7)));
                departement.setRegion(new Region(rs.getString(8),rs.getString(9)));
                liste.add(departement);}
   else {
                departement.getVilleListOfD().add(new Ville(rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getFloat(6), rs.getFloat(7)));
            }
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données departement impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Departement objet) {
        return false;
    }

    @Override
    public boolean update(Departement objet) {
        return false;
    }

    @Override
    public boolean delete(Departement objet) {
        return false;
    }
}
