package DAO;

import metier.Departement;
import metier.Disponibilite;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DisponibiliteDAO extends DAO<Disponibilite> {
    public DisponibiliteDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Disponibilite getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Disponibilite> getAll() {
        ResultSet rs;
        ArrayList<Disponibilite> liste = new ArrayList<Disponibilite>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  id_disponibilite,to_char(TO_DATE(d.jour,'DD'), 'Day ') , heure_debut, heure_fin from disponibilite order by jour ";

            rs = stmt.executeQuery(strCmd);
            Format formatter = new SimpleDateFormat("DD");

            Format format = new SimpleDateFormat("HH");

           Disponibilite disponibilite ;
            while (rs.next())
            { disponibilite = new Disponibilite(rs.getInt(1), formatter.format(rs.getDate(2)),format.format(rs.getTime(3)),format.format(rs.getTime(4)));
                liste.add(disponibilite);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données disponibilité impossible  "+ error);
        }
        return liste;
    }

    @Override
    public boolean insert(Disponibilite objet) {
        return false;
    }

    @Override
    public boolean update(Disponibilite objet) {
        return false;
    }

    @Override
    public boolean delete(Disponibilite objet) {
        return false;
    }
}
