package service;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
@ManagedBean
@SessionScoped
public class BeanConnect {



        // Declare the JDBC objects.
        private static Connection connexion;

        private BeanConnect()
        {
            try {
                Class.forName("org.postgresql.Driver");
                connexion = DriverManager.getConnection("jdbc:postgresql://localhost/GITES", "najib", "AFPA2019");

            }  catch (SQLException | ClassNotFoundException e) {
                System.out.println("Connection failure.");
                e.printStackTrace();


            }
        }

        public static synchronized Connection getInstance()
        {
            if (connexion == null)
                new BeanConnect();
            return connexion;
        }

    }

