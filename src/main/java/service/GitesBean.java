package service;


import DAO.DAOFactory;

import metier.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;

@ManagedBean (name="gite")
@SessionScoped
public class GitesBean implements Serializable {

    private static final long serialVersionUID = 6081417964063918994L;
    private ArrayList<Gite> listeGites ;
    private ArrayList<Ville> listeVilles;
    private ArrayList<Departement> listeDepartements ;
    private ArrayList<Region> listeRegions ;
    private ArrayList<Gite> listGitesSearched ;
    private int idGiteSearched;
    private Gite giteSelected;
    private ArrayList listeValeursMaxMin ;
    private ArrayList<EquipementService> listeEquipementsSerice ;
    private ArrayList<String> radiusList;
    private ArrayList<String> liste;


    public ArrayList getListeValeursMaxMin() {
        return listeValeursMaxMin;
    }


    public GitesBean() {
        listeGites = DAOFactory.getGiteDAO().getAll();


    }

    public ArrayList<EquipementService> getListeEquipementsSerice() {
        return listeEquipementsSerice;
    }

    private ArrayList<String> getRadiusListe() {
        ArrayList<String> stringRadiusList = new ArrayList<>();
        stringRadiusList.add(0, "R en km");
        stringRadiusList.add(1, "10");
        stringRadiusList.add(2, "20");
        stringRadiusList.add(3, "30");
        stringRadiusList.add(4, "40");
        stringRadiusList.add(5, "50");
        return stringRadiusList;
    }

    public ArrayList<Gite> getListGitesSearched(GiteSearched giteSearched) {
        listGitesSearched = DAOFactory.getGiteDAO().getLike(giteSearched);
        return listGitesSearched;
    }

    public ArrayList<String> getListe() {
        return liste;
    }

    public ArrayList<String> getRadiusList() {
        return radiusList;
    }

    public ArrayList<Ville> getListeVilles() {
        return listeVilles;
    }

    public ArrayList<Departement> getListeDepartements() {
        return listeDepartements;
    }

    public ArrayList<Region> getListeRegions() {
        return listeRegions;
    }

    public ArrayList<Gite> getListeGites() {
        return listeGites;
    }

    public Gite getGiteSelected() {
        giteSelected = DAOFactory.getGiteDAO().getByID(idGiteSearched);
        return giteSelected;
    }

    public void setGiteSelected(Gite giteSelected) {
        this.giteSelected = giteSelected;
    }

    public int getIdGiteSearched() {
        return idGiteSearched;
    }

    public void setIdGiteSearched(int idGiteSearched) {
        this.idGiteSearched = idGiteSearched;
    }


    public ArrayList<Ville> getListVilleOfPostalCode(String codePostale) {
        return DAOFactory.getVilleDAO().getByPostalCode(codePostale);
    }

    public ArrayList<Ville> getVillesListOfR(String codeRegion) {
        return DAOFactory.getRegionDAO().addListCodeRegion(codeRegion);
    }

    public ArrayList<Ville> getVilleListOfD(Departement departementSelected) {
        return DAOFactory.getDepartementDAO().addvilleListofDepartement(departementSelected.getCodeDepartement());
    }
}
