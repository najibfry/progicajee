package service;


import metier.*;

import java.io.Serializable;
import java.math.BigDecimal;

public class GiteSearched implements Serializable {

    private String  nomGite;
    private int nombreChambresMax;
    private int nombreChambresMin;
    private int nombreCouchagesMax;
    private int nombreCouchagesMin;
    private String PrestationsSearched;
    private BigDecimal prixMax;
    private Ville ville;
    private Departement departement;
    private Region region;
    private BigDecimal prixMin;
    private int idEquipementService;
    private String rayon;
    public String getRayon() {
        return rayon;
    }

    public GiteSearched setRayon(String rayon) {
        this.rayon = rayon;
        return this;
    }


    public GiteSearched() {
        this.nomGite = "";
    }

    public String getNomGite() {
        return nomGite;
    }

    public void setNomGite(String nomGite) {
        this.nomGite = nomGite;
    }

    public int getNombreChambresMax() {
        return nombreChambresMax;
    }

    public void setNombreChambresMax(int nombreChambresMax) {
        this.nombreChambresMax = nombreChambresMax;
    }

    public int getNombreChambresMin() {
        return nombreChambresMin;
    }

    public void setNombreChambresMin(int nombreChambresMin) {
        this.nombreChambresMin = nombreChambresMin;
    }

    public int getNombreCouchagesMax() {
        return nombreCouchagesMax;
    }

    public void setNombreCouchagesMax(int nombreCouchagesMax) {
        this.nombreCouchagesMax = nombreCouchagesMax;
    }

    public int getNombreCouchagesMin() {
        return nombreCouchagesMin;
    }

    public void setNombreCouchagesMin(int nombreCouchagesMin) {
        this.nombreCouchagesMin = nombreCouchagesMin;
    }

    public BigDecimal getPrixMax() {
        return prixMax;
    }

    public void setPrixMax(BigDecimal prixMax) {
        this.prixMax = prixMax;
    }



    public BigDecimal getPrixMin() {
        return prixMin;
    }

    public void setPrixMin(BigDecimal prixMin) {
        this.prixMin = prixMin;
    }

    public int getIdEquipementService() {
        return idEquipementService;
    }

    public void setIdEquipementService(int idEquipementService) {
        this.idEquipementService = idEquipementService;
    }

    public Ville getVille() {
        return ville;
    }

    public GiteSearched setVille(Ville ville) {
        this.ville = ville;
        return this;
    }

    public Departement getDepartement() {
        return departement;
    }

    public GiteSearched setDepartement(Departement departement) {
        this.departement = departement;
        return this;
    }

    public Region getRegion() {
        return region;
    }

    public GiteSearched setRegion(Region region) {
        this.region = region;
        return this;
    }

    public String getPrestationsSearched() {
        return PrestationsSearched;
    }

    public GiteSearched setPrestationsSearched(String prestationsSearched) {
        PrestationsSearched = prestationsSearched;
        return this;
    }

    @Override
    public String toString() {
        return "GiteSearched{" +
                "nomGite='" + nomGite + '\'' +
                ", nombreChambresMax=" + nombreChambresMax +
                ", nombreChambresMin=" + nombreChambresMin +
                ", nombreCouchagesMax=" + nombreCouchagesMax +
                ", nombreCouchagesMin=" + nombreCouchagesMin +
                ", prixMax=" + prixMax +
                ", ville=" + ville +
                ", departement=" + departement +
                ", region=" + region +
                ", prixMin=" + prixMin +
                ", idEquipementService=" + idEquipementService +
                '}';
    }
}






