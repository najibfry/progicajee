package metier;
import java.util.ArrayList;


public class Gite {
	private int idGite;
	private String nomGite;
	private int nombreChambres;
	private int nombreCouchages;
	private String siteWeb;
	private String adresse1;
	private String adresse2;
	private String adresse3;
	private String libelleVoie;
	private String codePostale;
	private Float surfaceHabitable;
	private ArrayList<Personne> proprietaires;
	private ArrayList<Personne> gerants;
	private Ville ville;
	private Tarification  tarif;
	private ArrayList<Prestation> prestations;


	public Gite(int idGite, String nomGite) {
		this.idGite =idGite;
		this.nomGite = nomGite;
	   this.proprietaires = new ArrayList<Personne>();
        this.gerants =  new ArrayList<Personne>();
		this.prestations = new ArrayList<Prestation>();
	}

	public int getIdGite() {
		return idGite;
	}

	public void setIdGite(int idGite) {
		this.idGite = idGite;
	}

	public String getNomGite() {
		return nomGite;
	}

	public void setNomGite(String nomGite) {
		this.nomGite = nomGite;
	}

	public int getNombreChambres() {
		return nombreChambres;
	}

	public void setNombreChambres(int nombreChambres) {
		this.nombreChambres = nombreChambres;
	}

	public int getNombreCouchages() {
		return nombreCouchages;
	}

	public void setNombreCouchages(int nombreCouchages) {
		this.nombreCouchages = nombreCouchages;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getAdresse1() {
		return adresse1;
	}

	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	public String getAdresse2() {
		return adresse2;
	}

	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	public String getAdresse3() {
		return adresse3;
	}

	public void setAdresse3(String adresse3) {
		this.adresse3 = adresse3;
	}

	public String getLibelleVoie() {
		return libelleVoie;
	}

	public void setLibelleVoie(String libelleVoie) {
		this.libelleVoie = libelleVoie;
	}

	public String getCodePostale() {
		return codePostale;
	}

	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}

	public Float getSurfaceHabitable() {
		return surfaceHabitable;
	}

	public void setSurfaceHabitable(Float surfaceHabitable) {
		this.surfaceHabitable = surfaceHabitable;
	}

	public ArrayList<Personne> getProprietaires() {
		return proprietaires;
	}

	public void setProprietaires(ArrayList<Personne> proprietaires) {
		this.proprietaires = proprietaires;
	}

	public ArrayList<Personne> getGerants() {
		return gerants;
	}

	public void setGerants(ArrayList<Personne> gerants) {
		this.gerants = gerants;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public Tarification getTarif() {
		return tarif;
	}

	public void setTarif(Tarification tarif) {
		this.tarif = tarif;
	}

	public ArrayList<Prestation> getPrestations() {
		return prestations;
	}

	public void setPrestations(ArrayList<Prestation> prestations) {
		this.prestations = prestations;
	}

	@Override
	public String toString() {
		return idGite +" " +
				 nomGite +" "+
				nombreChambres +" " +
				 nombreCouchages +" " +
				adresse1 +" " +
				 adresse2  +" "+
				 adresse3  +" "+
				 libelleVoie +" "
				+ codePostale  +" "+
				 surfaceHabitable +" "
				;


	}
}


