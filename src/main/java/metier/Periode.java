package metier;



import java.util.ArrayList;

public class Periode {
    private int idPeriode;
    private String numeroSemaine;
    private TypePeriode typePeriode;

	public Periode(int idPeriode, String numeroSemaine) {
		this.idPeriode = idPeriode;
		this.numeroSemaine = numeroSemaine;
		this.typePeriode = typePeriode;
	}

	public int getIdPeriode() {
		return idPeriode;
	}

	public Periode setIdPeriode(int idPeriode) {
		this.idPeriode = idPeriode;
		return this;
	}

	public String getNumeroSemaine() {
		return numeroSemaine;
	}

	public Periode setNumeroSemaine(String numeroSemaine) {
		this.numeroSemaine = numeroSemaine;
		return this;
	}

	public TypePeriode getTypePeriode() {
		return typePeriode;
	}

	public Periode setTypePeriode(TypePeriode typePeriode) {
		this.typePeriode = typePeriode;
		return this;
	}

	@Override
	public String toString() {
		return  numeroSemaine ;
	}
}
