package metier;

import java.util.ArrayList;

public class Departement {
    private String  codeDepartement;
    private String nomDepartement;
    private Region region;
    private ArrayList<Ville> villeListOfD;

    public Departement(String codeDepartement, String nomDepartement) {
        this.codeDepartement = codeDepartement;
        this.nomDepartement = nomDepartement;
        this.region= region;
        this.villeListOfD = new ArrayList<>();
    }

    public ArrayList<Ville> getVilleListOfD() {
        return villeListOfD;
    }

    public void setVilleListOfD(ArrayList<Ville> villeListOfD) {
        this.villeListOfD = villeListOfD;
    }

    public String getCodeDepartement() {
        return codeDepartement;
    }

    public Departement setCodeDepartement(String codeDepartement) {
        this.codeDepartement = codeDepartement;
        return this;
    }

    public String getNomDepartement() {
        return nomDepartement;
    }

    public Departement setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
        return this;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return nomDepartement ;
    }
}
